# VCinema  

[![Build Status](https://drone.jacknet.io/api/badges/TerribleCodeClub/VCinema/status.svg?ref=refs/heads/master)](https://drone.jacknet.io/TerribleCodeClub/VCinema)  

Web app for VCinema. 

[Live demo](https://vcinemaapp.b-cdn.net/)  

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

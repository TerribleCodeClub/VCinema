import { createApp } from 'vue'
import App from './App.vue'
import Router from './router'

// Import global components.
import FileSelect from '@/components/FileSelect.vue'
import WireStatistics from '@/components/WireStatistics.vue'

// Import PrimeVue dependencies.
import PrimeVue from 'primevue/config'
import Button from 'primevue/button'
import Card from 'primevue/card'
import Column from 'primevue/column'
import DataTable from 'primevue/datatable'
import Divider from 'primevue/divider'
import InputText from 'primevue/inputtext'
import MenuBar from 'primevue/menubar'
import ProgressBar from 'primevue/progressbar'
import ProgressSpinner from 'primevue/progressspinner'
import Toast from 'primevue/toast'
import ToastService from 'primevue/toastservice'
import 'primeflex/primeflex.css'
import 'primeicons/primeicons.css'
import 'primevue/resources/primevue.min.css'
import 'primevue/resources/themes/bootstrap4-light-blue/theme.css'

const app = createApp(App)

app.use(Router)
app.use(PrimeVue)
app.use(ToastService)

// Register PrimeVue components.
app.component('Button', Button)
app.component('Card', Card)
app.component('Column', Column)
app.component('DataTable', DataTable)
app.component('Divider', Divider)
app.component('InputText', InputText)
app.component('MenuBar', MenuBar)
app.component('ProgressBar', ProgressBar)
app.component('ProgressSpinner', ProgressSpinner)
app.component('Toast', Toast)

// Register VCinemaApp components.
app.component('FileSelect', FileSelect)
app.component('WireStatistics', WireStatistics)

app.provide('trackers', [
  'wss://tracker.lon.gb.jacknet.io/announce',
  'wss://tracker.man.gb.jacknet.io/announce'
])

app.provide('rtcConfig', {
  iceServers: [
    {
      urls: [
        'stun:relay.lon.gb.jacknet.io:5349',
        'stun:relay.man.gb.jacknet.io:5349',
        'turn:relay.lon.gb.jacknet.io:5349?transport=udp',
        'turn:relay.man.gb.jacknet.io:5349?transport=udp'
      ],
      username: 'vcinema',
      credential: '1121b3ecc5596b204147d0404d1df4e4a91858dcfd13c9e63781aa1e8f5f8f2c'
    }
  ],
  sdpSemantics: 'unified-plan'
})

// Render to DOM.
app.mount('#app')

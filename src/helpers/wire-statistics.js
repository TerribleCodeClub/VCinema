import prettyBytes from 'pretty-bytes'
import sha1 from 'sha-1'

const updateWireStatistics = (wires, wireStatistics) => {
  wires.forEach(wire => {
    if (!wireStatistics.some(w => w.wire === wire)) {
      wireStatistics.push({
        id: sha1(wire.peerId).toString().substring(0, 5),
        remoteAddress: wire.remoteAddress,
        wire: wire
      })
    }
  })
  wireStatistics.forEach(wireStatistic => {
    var wire = wireStatistic.wire
    wireStatistic.uploaded = prettyBytes(wire.uploaded)
    wireStatistic.downloaded = prettyBytes(wire.downloaded)
    wireStatistic.uploadSpeed = prettyBytes(wire.uploadSpeed()) + 'ps'
    wireStatistic.downloadSpeed = prettyBytes(wire.downloadSpeed()) + 'ps'
  })
}

export default updateWireStatistics


const { EventEmitter } = require('events')
const bencode = require('bencode')

const EXTENSION_NAME = 'vcinema'

class VCinemaWireExtension extends EventEmitter {
  static _alias = 'Unknown'

  constructor (wire) {
    super()
    this._ready = false
    this._wire = wire
    this._remoteAlias = 'Unknown'
    this.emit('info', `Extended handshake using ${this._alias}.`)
    wire.extendedHandshake[`${EXTENSION_NAME}_alias`] = Buffer.from(VCinemaWireExtension._alias)
  }

  get name () {
    return EXTENSION_NAME
  }

  get ready () {
    return this._ready
  }

  get wire () {
    return this._wire
  }

  get remoteAlias () {
    return this._remoteAlias
  }

  static get alias () {
    return VCinemaWireExtension._alias
  }

  static set alias (alias) {
    VCinemaWireExtension._alias = alias
  }

  onExtendedHandshake (handshake) {
    if (!handshake.m || !handshake.m[EXTENSION_NAME]) {
      this.emit('info', `Peer does not support ${EXTENSION_NAME}.`)
      return
    }

    this._ready = true
    this._remoteAlias = handshake[`${EXTENSION_NAME}_alias`] || 'Unknown'
    this.emit('info', `Peer alias set as ${this._remoteAlias}.`)
    this.emit('alias', this._remoteAlias)
  }

  onMessage (buffer) {
    const decodedBuffer = this._decode(buffer)
    this.emit('info', `Message received from ${this._remoteAlias}.`)
    this.emit('message', decodedBuffer)
  }

  send (message) {
    this.emit('info', `Sending message to ${this._remoteAlias}.`)
    this._send(message)
  }

  _encode (message) {
    return bencode.encode(JSON.stringify(message))
  }

  _decode (buffer) {
    return JSON.parse(bencode.decode(buffer.toString()))
  }

  _send (message) {
    this.wire.extended(EXTENSION_NAME, this._encode(message))
  }
}

module.exports = VCinemaWireExtension

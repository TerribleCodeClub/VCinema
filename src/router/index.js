import { createWebHistory, createRouter } from 'vue-router'
import Home from '@/views/Home.vue'
import Host from '@/views/Host.vue'
import Join from '@/views/Join.vue'
import Screen from '@/views/Screen.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/host',
    name: 'Host',
    component: Host
  },
  {
    path: '/join',
    name: 'Join',
    component: Join
  },
  {
    path: '/join/:id',
    name: 'Screen',
    component: Screen,
    props: true
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
